# README #

This is a simple Android application which demonstrates easy to use features.

### How do I get set up? ###

* IntelliJ 2016.3 or Android Studio 2.x


### Demonstrated Features ###

* Capturing an image via the camera without holding the `android.Manifest.permission.CAMERA` permission
* Getting an image from existing media without holding any special permissions

The permission `android.Manifest.permission.WRITE_EXTERNAL_STORAGE` is only used for API levels below 18.  After that API version an app does not need the permission to access its private area on external storage.

