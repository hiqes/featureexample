package com.hiqes.android.featureexample;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "FeatureExample";
    private static final int    REQ_CODE_GET_IMG = 1;

    private ImageView mCameraImg;
    private Uri mCapUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mCameraImg = (ImageView)findViewById(R.id.camera_img);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent contentIntent = new Intent(Intent.ACTION_GET_CONTENT);
                contentIntent.addCategory(Intent.CATEGORY_OPENABLE);
                contentIntent.setTypeAndNormalize("image/*");
                contentIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                try {
                    File captureOutput =
                            File.createTempFile("ljscap",
                                                ".jpg",
                                                getExternalFilesDir(Environment.DIRECTORY_PICTURES));

                    mCapUri = FileProvider.getUriForFile(MainActivity.this,
                                                         "com.hiqes.android.featureexample.fileprovider",
                                                         captureOutput);
                    captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapUri);
                    captureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                    Log.d(TAG, "Requesting capture to: " + mCapUri.toString());
                } catch (IOException e) {
                    //  pass through
                }

                List<Intent> intents = new ArrayList<>();
                intents.add(captureIntent);

                Log.d(TAG, "Firing Intent to pick image");
                Intent chooserIntent = Intent.createChooser(contentIntent, "Load an image");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intents.toArray(new Parcelable[intents.size()]));
                startActivityForResult(chooserIntent, REQ_CODE_GET_IMG);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Uri                     resultUri = null;
        Bitmap                  resultBitmap = null;

        if (resultCode == Activity.RESULT_OK) {
            if ((data == null) && (mCapUri != null)) {
                resultUri = mCapUri;
            } else {
                resultUri = data.getData();

                if (resultUri == null) {
                    resultUri = mCapUri;
                    if (resultUri != null) {
                        Log.d(TAG, "Got result at: " + resultUri.toString());
                    }
                }

                if (resultUri == null) {
                    Bundle extras = data.getExtras();
                    resultBitmap = (Bitmap) extras.get("data");
                }
            }

            if (resultUri != null) {
                Log.d(TAG, "Received result, data at: " + resultUri.toString());
                Picasso.with(this).load(resultUri).into(mCameraImg);
            } else if (resultBitmap != null) {
                Log.d(TAG, "Received result data in intent");
                mCameraImg.setImageBitmap(resultBitmap);
            } else {
                Snackbar.make(findViewById(R.id.content_main), R.string.err_no_img_data, Snackbar.LENGTH_LONG).show();
            }
        } else {
            Log.d(TAG, "Failed to get image");
            Snackbar.make(findViewById(R.id.content_main), R.string.err_no_img_selected, Snackbar.LENGTH_LONG).show();
        }
    }
}
